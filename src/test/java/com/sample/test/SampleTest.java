package com.sample.test;

import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;

import com.sample.config.WebMvcConfig;
import com.sample.controller.DemoController;

import static org.junit.Assert.assertTrue;

import org.junit.Test;

@RunWith(SpringRunner.class)
public class SampleTest {
	
	@Test
	public void test() {
		System.out.println("Testing..........");
		new DemoController().welcome(null);
		
		WebMvcConfig webMvcConfig = new WebMvcConfig();
		webMvcConfig.messageSource();
		webMvcConfig.getValidator();
		webMvcConfig.localeChangeInterceptor();
		webMvcConfig.getLocaleResolver();
		webMvcConfig.characterEncodingFilter();
		assertTrue("Test condition is true", true);
	}

}
