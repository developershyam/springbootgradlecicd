package com.sample.integration.zerocode;

import org.jsmart.zerocode.core.domain.TargetEnv;
import org.jsmart.zerocode.core.domain.TestPackageRoot;
import org.jsmart.zerocode.core.runner.ZeroCodePackageRunner;
import org.junit.runner.RunWith;

@TargetEnv("app-test.properties")
@RunWith(ZeroCodePackageRunner.class)
@TestPackageRoot("integration") //<--- Root of the package to pick all tests including sub-folders
public class ZeroCodeSuite {
}
