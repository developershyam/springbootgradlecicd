package com.sample.integration.restassured;

import static org.hamcrest.CoreMatchers.equalTo;

import org.junit.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

public class RestAssuredGetTest {

	@Test
	public void GetWeatherDetails()
	{   
		RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

		RestAssured.get("/Pune").then().assertThat()
		.statusCode(200)
		.body("City", equalTo("Pune"))
		.body("Humidity", equalTo("98 Percent"));
	}
	
	@Test
	public void GetWeatherDetailstest()
	{   
		// Specify the base URL to the RESTful web service
		RestAssured.baseURI = "http://restapi.demoqa.com/utilities/weather/city";

		// Get the RequestSpecification of the request that you want to sent
		// to the server. The server is specified by the BaseURI that we have
		// specified in the above step.
		RequestSpecification httpRequest = RestAssured.given();

		// Make a request to the server by specifying the method Type and the method URL.
		// This will return the Response from the server. Store the response in a variable.
		Response response = httpRequest.request(Method.GET, "/Pune");

		System.out.println(response.statusCode());
		// Now let us print the body of the message to see what response
		// we have recieved from the server
		String responseBody = response.getBody().asString();
		System.out.println("Response Body is =>  " + responseBody);
	}

}
